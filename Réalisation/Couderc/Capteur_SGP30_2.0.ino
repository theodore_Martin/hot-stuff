#include <Wire.h>               // inclue la biblioteque Wire pour la com I2C
#include "Adafruit_SGP30.h"     // inclue la bibliothèque Adafruit_SGP30 pour l'interaction avec le capteur SGP30

Adafruit_SGP30 sgp;

void setup() {                  // initialise la communication série
  Serial.begin(9600);
  while (!Serial) {
    delay(100); 
  }

  if (!sgp.begin()){                             // verifie que le capteur soit détecté
    Serial.println("Sensor not found :(");  
    while (1);
  }

 
              }

void loop() {
  if (! sgp.IAQmeasure()) {                       // Teste de mesure
    Serial.println("Measurement failed");
    return;
  }

  Serial.print("TVOC "); Serial.print(sgp.TVOC);
  Serial.print(" ppb\t");
  Serial.print("eCO2 "); Serial.print(sgp.eCO2);
  Serial.println(" ppm");                         // affiche les valeurs de CO2 et COV        

  delay(1000);
}
