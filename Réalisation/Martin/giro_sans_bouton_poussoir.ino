int gyrophare=A0;

void setup() {
  pinMode(A0, OUTPUT); // defini la broche du gyrophare en sortie
}

void loop () {
  digitalWrite(gyrophare, HIGH); // mets la sortie du gyro en haut (+12V)
  delay(2000);  // attendre 0,5 seconde
  digitalWrite(gyrophare, LOW); // mets la sortie du gyro en bas (0V)
  delay(2000);  // attendre 0,5 seconde
}
