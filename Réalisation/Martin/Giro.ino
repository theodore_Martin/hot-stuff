//button.ino - allume un gyrophare en appuyant sur un bouton
// (c) BotBook.com - Karvinen, Karvinen, Valtokari
int buttonPin=2;
int gyrophare=A0;
int buttonStatus=1;
void setup() {
  pinMode(gyrophare, OUTPUT);
  pinMode(buttonPin, INPUT); // 1
  digitalWrite(buttonPin, HIGH); // pull-up interne // 2

}
void loop () {
  buttonStatus=digitalRead(buttonPin); // 3
  if (LOW==buttonStatus) { // 4
    digitalWrite(gyrophare, HIGH); // 5
    } else {
      digitalWrite(gyrophare, LOW);
    }
    delay(20);  // 6
  }
