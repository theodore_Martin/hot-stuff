HautParleur 
{
  
#define pinDeSortieArduino 12   // On va utiliser la broche D12 de l'Arduino Uno
#define frequenceDeDebut 700    // Fréquence "basse" de la sirène
#define frequenceDeFin 2000     // Fréquence "haute" de la sirène

  void setup()
  {
   pinMode(pinDeSortieArduino, OUTPUT); // Définition de la broche D12 en "sortie"
  } 

  void loop()
  {
    // Phase de "montée" sirène
    for (int i = frequenceDeDebut; i < frequenceDeFin; i=i+3) 
    {
      tone(pinDeSortieArduino, i); 
      delay(7); // delay en ms
    }

    // Phase de "descente" sirène
    for (int i = frequenceDeFin; i > frequenceDeDebut; i=i-3) 
    {
      tone(pinDeSortieArduino, i); 
      delay(5); // delay en ms
    }
  }

}
