#include <Wire.h>  // inclue la biblioteque Wire pour la com I2C
#include <Tasks.h>
#include "Adafruit_SGP30.h"// inclue la bibliothèque Adafruit_SGP30 pour l'interaction avec le capteur SGP30

const int pinDeSortieArduino = 12;   // On va utiliser la broche D12 de l'Arduino Uno
#define frequenceDeDebut 700    // Fréquence "basse" de la sirène
#define frequenceArret 0
#define frequenceDeFin 2000     // Fréquence "haute" de la sirène
const int gyrophare = A0;
const byte interruptPin = 2;

volatile boolean alarmActive = false;
Adafruit_SGP30 sgp;





void setup()
{ // initialise la communication série
  alarmActive = false;
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), toggleAlarm, CHANGE); // activation de la fonction toggleAlarme
  pinMode(gyrophare, OUTPUT); // defini la broche du gyrophare en sortie
  pinMode(pinDeSortieArduino, OUTPUT); // Définition de la broche D12 en "sortie"
  Serial.begin(9600);
  while (!Serial)
  {
    delay(100);
  }

  if (!sgp.begin())// verifie que le capteur soit détecté
  { 
    Serial.println("Sensor not found :(");
    while (1);
  }

  Tasks_Init();
  Tasks_Add((Task) Gyro, 500, 0);
  Tasks_Add((Task) HP, 500, 0);
}




void HP() {
  if (alarmActive) {
    for (int i = frequenceDeDebut; i < frequenceDeFin; i = i + 3) // Phase de "montée" sirène
    {
      tone(pinDeSortieArduino, i);
      delay(7); // delay en ms
    }

    // Phase de "descente" sirène
    for (int i = frequenceDeFin; i > frequenceDeDebut; i = i - 3)
    {
      tone(pinDeSortieArduino, i);
      delay(5); // delay en ms
    }
  } else {
    for (int i = frequenceDeFin; i > frequenceArret; i = i - 3)
    noTone(pinDeSortieArduino);
  }
}




void Gyro() {
  if (alarmActive) {
    digitalWrite(gyrophare, !digitalRead(gyrophare));
  } else {
    digitalWrite(gyrophare, LOW);
  }
}





void activateAlarm()  //fonction pour activer HP et Gyro
{
  if (alarmActive) {
    Serial.println("Dangerous levels detected!");
    //alarmActive = true;
    Tasks_Start();

  }
}




void toggleAlarm() // fonction pour passer de alarme active à désactivée
{
  if (alarmActive)
  {
    Tasks_Pause();
    alarmActive = false;
  }
  
}





void loop()
{
  if (! sgp.IAQmeasure())
  { // Teste de mesure
    Serial.println("Measurement failed");
    return;
  }

  Serial.print("TVOC "); Serial.print(sgp.TVOC);
  Serial.print(" ppb\t");
  Serial.print("eCO2 "); Serial.print(sgp.eCO2);
  Serial.println(" ppm");
  // affiche les valeurs de CO2 et COV

  delay(1000);

  if (sgp.eCO2 > 800 || sgp.TVOC > 1000)  // si la concentration de CO2 est supérieure à 1200 ppm ou si la concentration de COV est supérieure à 3000 ppb
  {
    if (!alarmActive) {
      alarmActive = true;
      activateAlarm();
    }
  }

  

 if (Tasks_Pause)
  {
    digitalWrite(gyrophare, LOW); // éteint le gyrophare
    noTone(pinDeSortieArduino); // éteint la sirène
  }

  delay(1000);
   
}
