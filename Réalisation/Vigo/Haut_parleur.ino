#define pinSignalDeSortie B00010000   // Masque binaire, permettant de sélectionner la broche D12 de l'Arduino Uno
#define frequenceDeSortie 250// On entre ici la fréquence souhaitée sur cette sortie (2000 Hz par défaut)

#define periodeDuSignal (float)1/frequenceDeSortie*1000000   // Période = 1/Fréquence (et multiplié par 1 million, pour l'exprimer en micro-secondes)
#define tempo (float)periodeDuSignal/2    // délais calculés automatiquement, se rajoutant à l'état haut et à l'état bas, afin d'atteindre la fréquence souhaitée

void setup() {
  DDRB = DDRB | pinSignalDeSortie;        // Déclare la broche D12 comme étant une "sortie"
}
void loop() {
  PORTB = PORTB | pinSignalDeSortie;      // Met la sortie D12 à l'état haut (+5V)
  _delay_us(tempo-0.15);    // Ajoute un délai supplémentaire, avant de repasser à l'état bas
  PORTB = PORTB & ~pinSignalDeSortie;     // Met la sortie D12 à l'état bas (0V)
  _delay_us(tempo-0.4);     // Ajoute un délai supplémentaire, avant de repasser à l'état haut
} 
// … et on boucle indéfiniment !
